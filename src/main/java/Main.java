import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SQLHandler test = new SQLHandler();
        Scanner inputScanner = new Scanner(System.in);

        System.out.println("PLease enter customer ID (or simply type 0 for random customer): ");
        int userInput = inputScanner.nextInt();

        if (userInput > 0) {
            System.out.println(test.getCustomer(userInput));
            System.out.println(test.getCustomerMostPopularGenre(userInput));
        }
        else {
            System.out.println(test.getRandomCustomer());
        }
    }
}
