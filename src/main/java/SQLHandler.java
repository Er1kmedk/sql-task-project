import java.sql.*;
import java.util.*;


public class SQLHandler {

    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection connection;


    // A simple call to the Customer
    public String getCustomer(int requestedCustomersId) {
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT FirstName, LastName FROM Customer WHERE CustomerId=" + requestedCustomersId);
            ResultSet resultSet = preparedStatement.executeQuery();

            return "Firstname: " + resultSet.getString("FirstName") + "\nLastname: " + resultSet.getString("LastName");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            try {
                // Close Connection
                connection.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return null;
    }

    // The SQL queries in this method is systematically called level by level throughout the Chinook database
    // and the desired results are finally gathered in a Hashmap for clarity.
    // The out-commented System.out calls within each while loop, are used to track each step of the progress.
    public String getCustomerMostPopularGenre(int requestedCustomersId) {

        ArrayList<String> genreIds = new ArrayList<String>();
        HashMap<String, Integer> genreHashMap = new HashMap<String, Integer>();

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT InvoiceId FROM Invoice WHERE CustomerId=?");
            // The parameter index represent that # of '?' occurrence in previous line
            preparedStatement.setInt(1, requestedCustomersId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
//                System.out.println(resultSet.getString("InvoiceId"));

                PreparedStatement preparedStatementTwo = connection.prepareStatement("SELECT TrackId FROM InvoiceLine WHERE InvoiceId=?");
                preparedStatementTwo.setString(1, resultSet.getString("InvoiceId"));
                ResultSet resultSetTwo = preparedStatementTwo.executeQuery();

                while (resultSetTwo.next()) {
//                    System.out.println(resultSetTwo.getString("TrackId"));

                    PreparedStatement preparedStatementThree = connection.prepareStatement("SELECT GenreId FROM Track WHERE TrackId=?");
                    preparedStatementThree.setString(1, resultSetTwo.getString("TrackId"));
                    ResultSet resultSetThree = preparedStatementThree.executeQuery();

                    while (resultSetThree.next()) {
//                        System.out.println(resultSetThree.getString("GenreId"));

                        PreparedStatement preparedStatementFour = connection.prepareStatement("SELECT Name FROM Genre WHERE GenreId=?");
                        preparedStatementFour.setString(1, resultSetThree.getString("GenreId"));
                        ResultSet resultSetFour = preparedStatementFour.executeQuery();

                        while ((resultSetFour.next())){
//                            System.out.println(resultSetFour.getString("Name"));
                            genreIds.add(resultSetFour.getString("Name"));
                        }
                    }
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        // Within the finally section we close the connection as well as return the result
        // from the hashMap. All categories can be viewed but only the most popular was
        // requested in the assignment and therefore only that genre gets returned.
        finally {
            for (Object genreId : genreIds) {
                if (genreHashMap.containsKey(genreId)) {
                    genreHashMap.put(genreId.toString(), genreHashMap.get(genreId) + 1);
                }
                else {
                    genreHashMap.put(genreId.toString(), 1);
                }
            }

            int maxValueInMap=(Collections.max(genreHashMap.values()));
            for (Map.Entry<String, Integer> entry : genreHashMap.entrySet()) {
                if (entry.getValue() == maxValueInMap) {
                    return "Most popular genre: " + entry.getKey();
                }
            }

            try {
                // Close Connection
                connection.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return null;
    }

    public String getRandomCustomer() {
        ArrayList<String> availableCustomerIds = new ArrayList<String>();
        Random random = new Random();
        int randomCustomerId;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT CustomerId FROM Customer");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
//                System.out.println(resultSet.getString("customerId"));
                availableCustomerIds.add(resultSet.getString("customerId"));

            }

            randomCustomerId = Integer.parseInt(availableCustomerIds.get(random.nextInt(availableCustomerIds.size())));
            String answerString = "Random customerId: " + randomCustomerId + "\n";
            answerString += getCustomer(randomCustomerId) + "\n";
            answerString += getCustomerMostPopularGenre(randomCustomerId);
            return answerString;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            try {
                // Close Connection
                connection.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return null;
    }
}
